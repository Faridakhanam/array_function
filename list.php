<?php

$info = array('coffee', 'brown', 'caffeine');

list($drink, $color, $power) = $info;
echo "$drink is $color and $power makes it special.\n";
echo "<br>";
// Listing some of them
list($drink, , $power) = $info;
echo "$drink has $power.\n";
echo "<br>";

list($bar) = "abcde";
var_dump($bar); 
?>
